<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableBankHistorical extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_historical', function (Blueprint $table) {
            $table->id();
            $table->string('ref_code');
            $table->integer('back_id');
            $table->string('back_code');
            $table->integer('back_id_ref');
            $table->string('back_code_ref');
            $table->integer('type');
            $table->integer('type_in');
            $table->float('price', 11, 2);  
            $table->timestamps();
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
