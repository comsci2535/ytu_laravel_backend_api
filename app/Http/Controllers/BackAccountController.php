<?php

namespace App\Http\Controllers;

use App\Models\back_account;
use App\Models\Back_account_detail;
use Illuminate\Http\Request;

class BackAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $back_account = back_account::all(); 
        if(!empty($back_account)):
            foreach($back_account as $item):
                $objDetail = Back_account_detail::where('back_id',$item->id)->first(); 
                $item->price = !empty($objDetail->price)? $objDetail->price : 0;
            endforeach;
        endif;
        
        $data['data'] = $back_account;

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $back_account = new back_account();
        $back_account->back_code = $request->back_code;
        $back_account->back_name = $request->back_name;
        $back_account->save(); 
        if($back_account):
            $id = $back_account->id;
            $detail = new Back_account_detail();
            $detail->back_id = $id;
            $detail->back_code = $request->back_code;
            $detail->price     = $request->price;
            $detail->save(); 
            $data['data'] = $back_account;
            $data['msg'] = "บันทึกสำเร็จ";
        else:
            $data['msg'] = "ไม่สามารถบันทึกข้อมูลได้";
        endif;

        return response()->json($data);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\back_account  $back_account
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $back_account = back_account::find($id); 
        $objDetail = Back_account_detail::where('back_id',$back_account->id)->first(); 
        $back_account->price = !empty($objDetail->price)? $objDetail->price : 0;
        $data['data'] = $back_account;
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\back_account  $back_account
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $back_account            = back_account::find($id);
        $back_account->back_code = $request->back_code;
        $back_account->back_name = $request->back_name;
        $back_account->save(); 
        if($back_account):
            $objDetail = Back_account_detail::where('back_id',$back_account->id)->first(); 
            $objDetail->price = $request->price;
            $objDetail->save(); 
            $data['data'] = $back_account;
            $data['msg'] = "แก้ไขข้อมูลสำเร็จ";
        else:
            $data['msg'] = "ไม่สามารถแก้ไขข้อมูลได้";
        endif;

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\back_account  $back_account
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $back_account            = back_account::destroy($id);
        if($back_account):
            $data['msg']   = 'ลบข้อมูลเรียบร้อย';
        else:
            $data['msg']   = 'ลบข้อมูลไม่สำเร็จ';
        endif;

        return response()->json($data);
    }
}
