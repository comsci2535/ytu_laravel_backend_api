<?php

namespace App\Http\Controllers;

use App\Models\Bank_historical;
use Illuminate\Http\Request;

class BankHistoricalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bank_historical = Bank_historical::all(); 
        $data['data'] = $bank_historical;

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $bank_historical = new Bank_historical();
        $bank_historical->ref_code = $request->ref_code;
        $bank_historical->back_id = $request->back_id;
        $bank_historical->back_code = $request->back_code;
        $bank_historical->back_id_ref = $request->back_id_ref;
        $bank_historical->back_code_ref = $request->back_code_ref;
        $bank_historical->type = $request->type;
        $bank_historical->type_in = $request->type_in;
        $bank_historical->price = $request->price; 
        $bank_historical->save(); 
        if($bank_historical):
            $data['data'] = $bank_historical;
            $data['msg'] = "บันทึกสำเร็จ";
        else:
            $data['msg'] = "ไม่สามารถบันทึกข้อมูลได้";
        endif;

        return response()->json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Bank_historical  $bank_historical
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $bank_historical = Bank_historical::find($id); 
        $data['data'] = $bank_historical;
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Bank_historical  $bank_historical
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $bank_historical            = Bank_historical::find($id);
        $bank_historical->ref_code = $request->ref_code;
        $bank_historical->back_id = $request->back_id;
        $bank_historical->back_code = $request->back_code;
        $bank_historical->back_id_ref = $request->back_id_ref;
        $bank_historical->back_code_ref = $request->back_code_ref;
        $bank_historical->type = $request->type;
        $bank_historical->type_in = $request->type_in;
        $bank_historical->price = $request->price; 
        $bank_historical->save(); 
        if($bank_historical):
            $data['data'] = $bank_historical;
            $data['msg'] = "แก้ไขข้อมูลสำเร็จ";
        else:
            $data['msg'] = "ไม่สามารถแก้ไขข้อมูลได้";
        endif;

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Bank_historical  $bank_historical
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bank_historical     = Bank_historical::destroy($id);
        if($bank_historical):
            $data['msg']   = 'ลบข้อมูลเรียบร้อย';
        else:
            $data['msg']   = 'ลบข้อมูลไม่สำเร็จ';
        endif;

        return response()->json($data);
    }
}
