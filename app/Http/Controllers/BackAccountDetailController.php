<?php

namespace App\Http\Controllers;

use App\Models\Back_account_detail;
use Illuminate\Http\Request;

class BackAccountDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $back_account_detail = Back_account_detail::all(); 
        $data['data'] = $back_account_detail;

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $back_account_detail = new Back_account_detail();
        $back_account_detail->back_code = $request->back_code;
        $back_account_detail->back_name = $request->back_name;
        $back_account_detail->save(); 
        if($back_account_detail):
            $data['data'] = $back_account_detail;
            $data['msg'] = "บันทึกสำเร็จ";
        else:
            $data['msg'] = "ไม่สามารถบันทึกข้อมูลได้";
        endif;

        return response()->json($data);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Back_account_detail  $back_account_detail
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $back_account_detail = Back_account_detail::find($id); 
        $data['data'] = $back_account_detail;
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Back_account_detail  $back_account_detail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $back_account_detail            = Back_account_detail::find($id);
        $back_account_detail->back_code = $request->back_code;
        $back_account_detail->back_name = $request->back_name;
        $back_account_detail->save(); 
        if($back_account_detail):
            $data['data'] = $back_account_detail;
            $data['msg'] = "แก้ไขข้อมูลสำเร็จ";
        else:
            $data['msg'] = "ไม่สามารถแก้ไขข้อมูลได้";
        endif;

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Back_account_detail  $back_account_detail
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $back_account_detail            = Back_account_detail::destroy($id);
        if($back_account_detail):
            $data['msg']   = 'ลบข้อมูลเรียบร้อย';
        else:
            $data['msg']   = 'ลบข้อมูลไม่สำเร็จ';
        endif;

        return response()->json($data);
    }
}
