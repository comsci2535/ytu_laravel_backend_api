<?php

use App\Http\Controllers\BackAccountController;
use App\Http\Controllers\BackAccountDetailController;
use App\Http\Controllers\BankHistoricalController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('back_account', BackAccountController::class);
Route::apiResource('back_account_detail', BackAccountDetailController::class);
Route::apiResource('bank_historical', BankHistoricalController::class);